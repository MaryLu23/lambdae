/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MARU
 */
public class LambaTestO {
    public static void main(String[] args){
        //Operaciones op =(numero1, numero2)-> System.out.println(numero1 + numero2);
        //op.impirmir(5, 10);
        
        //otra forma
        //Operaciones op =(numero1, numero2)-> System.out.println(numero1 + numero2);
        //LambaTestO objeto = new LambaTestO();
        //objeto.Metodo(op, 10,10);
        
        //
        Operaciones op =(numero1, numero2)-> System.out.println(numero1 + numero2);
        op.impirmir(5, 10);
        LambaTestO objeto = new LambaTestO();
        objeto.Metodo((numero1, numero2)->System.out.println(numero1 - numero2),20,10);
        objeto.Metodo((numero1, numero2)->System.out.println(numero1 * numero2),20,10);
    }
    public void Metodo(Operaciones op, int numero1, int numero2){
        op.impirmir(numero1, numero2);
    }
}
